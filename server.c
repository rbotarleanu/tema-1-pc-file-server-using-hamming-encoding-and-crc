//Nume: Botarleanu Robert-Mihai
//Grupa: 321CB
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/stat.h>

#include "lib.h"

#define HOST "127.0.0.1"
#define PORT 10001

typedef unsigned char byte;
typedef unsigned short word;

void parityEncode(msg *t);
void hammingEncode(msg *t);
void hammingDecode(msg *t);

int loadAndSend(char *fname, int errorMode);
int getAndWrite(char *fname, int errorMode);

int waitForCorrectMessage(msg *r, int errorMode);
int stopAndWait(msg t, int errorMode);

char **listFiles(char *path, int *size);
int sendLs(char **files, int fCount, int errorMode); 

enum {NONE, PARITY, HAMMING};	

//trimite o confirmare la client
//va intoarce 0 daca confirmarea este trimisa cu succes
int acknowledge() {
	msg t;
	sprintf(t.payload, "ACK");
	t.len = strlen(t.payload) + 1;
	int res = send_message(&t);

	if(res < 0) {
		perror("[RECEIVER] Receive error. Exiting.\n");
				return -1;
	}
	return 0;
}

//trimite o confirmare negativa la client
//va intoarce 0 daca aceasta este trimisa cu succes
int nacknowledge() {
	msg t;
	sprintf(t.payload, "NACK");
	t.len = strlen(t.payload) + 1;
	int res = send_message(&t);

	if(res < 0) {
		perror("[RECEIVER] Receive error. Exiting.\n");
				return -1;
	}
	return 0;
}

//afla bitul de rank i(de la 0 la 7, unde 0 o sa returneze bitul cel mai putin semnificativ)
byte getBit(byte c, int rank){
	return c & (1<<rank);
}

//seteaza bitul de rank i dintr-un octet
void setBit(byte *c, int rank){
	(*c) |= (1<<rank);
}


//aceleasi functii pentru word, bitul cel mai semnificativ fiind pe rank-ul 0
byte getBitWord(word c, int rank){
	return (c>>rank) & 1;
}

//seteaza bitul de rank i (de la 0 la 15)
void setBitWord(word *c, int rank){
	word aux = 1; 
	aux <<= rank;
	(*c) |= aux;
}

//complementeaza bitul de rank i (de la 0 la 15)
void complementBitWord(word *c, int rank){
	word aux = 1;
	aux <<= rank;
	(*c) ^= aux;
}

//verifica daca pozitia e controlata de un bit
byte ctrlPos(int x, int rank){
	return x & (1<<(rank-1));
}


//asteapta confirmare de la client
//returneaza 0 daca e ACK, 1 daca e NACK si -1 daca are loc o eroare
int getAck(msg *r, int errorMode){
	int res = recv_message(r);
	if(res < 0){
		perror("Receive error.\n");
		return -1;
	}
	int correct = 1;
	fflush(stdout);
	if(errorMode == PARITY) correct = ((*r).len == 4);
	if(correct) return 0; 
	return 1;
}

int main(int argc, char **argv)
{
	char *path = NULL;
	int errorMode = NONE;

	if(argc == 2) {
		//modul de corectare a erorii
		if(!strcmp(argv[1], "parity")) errorMode = PARITY;
		else if(!strcmp(argv[1], "hamming")) errorMode = HAMMING;
	}

	msg r;
	int i;
	printf("[RECEIVER] Starting.\n");
	init(HOST, PORT);
	
	for (i = 0; i < COUNT; i++) {
		printf("%d\n\n", i);
		// asteapta un mesaj corect 
		if(waitForCorrectMessage(&r, errorMode)) return -1;

		char *s = strtok(r.payload, " ");
		
		//cd
		if(strcmp(s, "cd") == 0) {
			int response = chdir(strtok(0," "));
			if(response) return response;
			acknowledge();
		} 

		//ls
		else if(strcmp(s, "ls") == 0) {
			int fCount = 0;
			if(path != NULL) free(path);
			path = strdup(strtok(0, " "));
			char **files = listFiles(path, &fCount);
			int response = sendLs(files, fCount, errorMode);
			if(response) return response;
		}

		//cp
		else if(strcmp(s, "cp") == 0) {
			char *fname = strdup(strtok(0, " "));
			acknowledge();
			int response = loadAndSend(fname, errorMode);
			free(fname);
			if(response) return response;
		}

		//sn
		else if(strcmp(s, "sn") == 0) {
			char fname[100] = "new_";
			strcat(fname, strtok(0, " "));
			acknowledge();
			getAndWrite(fname, errorMode);
		}

		//exit
		else if(!strcmp(s, "exit") && !strcmp(strtok(0, " "), "exit")) {
			acknowledge();
			break;
		}
	}

	printf("[RECEIVER] Finished receiving..\n");
	return 0;
}

//trimite raspusul serverului la comanda ls
int sendLs(char **files, int fCount, int errorMode){
	msg t;
	int i ,response = 0;

	//confirma receptionarea comenzii
	response = acknowledge();	
	if(response) return response;
	
	//trimite numarul de fisiere
	sprintf(t.payload, "%d", fCount);
	t.len = strlen(t.payload) + 1;
	response = stopAndWait(t, errorMode);
	if(response) return response;

	//trimite numele fisierelor
	for (i = 0; i < fCount; ++i)
	{
		t.payload[0] = '\0';
		sprintf(t.payload, "%s", files[i]);
		t.len = strlen(t.payload) + 1;
		fflush(stdout);
		response = stopAndWait(t, errorMode);
		if(response) return response; 
	}
	return 0;
}


//codeaza mesajul in functie de modul de corectie al erorilor folosit
void encodeMessage(msg *t, int errorMode){
	if(errorMode == PARITY) parityEncode(t);
	else if(errorMode == HAMMING) hammingEncode(t);
}

//trimite mesajul t si asteapta ACK
//daca clientul nu raspunde cu ACK, retrimite mesajul
int stopAndWait(msg t, int errorMode) {
	msg r;
	int res;
	//codeaza mesajul in functie de modul de corectie al erorilor folosit
	encodeMessage(&t, errorMode);	
	
	while(1) {
		res = send_message(&t);
		if (res < 0) {
				perror("[RECEIVER] Receive error. Exiting.\n");
				return -1;
		}
		res = getAck(&r, errorMode);
		if( res < 0) {
			perror("[RECEIVER] Receive error. Exiting.\n");
			fflush(stdout);
			return -1;	
		}
		if(!res) break;
	}
	return 0;

}

//calculeaza suma de paritate pentru secventa de octeti s de lungime len
//valoarea returnata va fi 0 sau 1
byte calculateParitySum(const char *s, int len){
	byte sum = 0;
	int k = 0;
	while(k < len){
		int i = 0;
		for(; i <= 7; i++)
			if(getBit(*s, i)) sum ^= 1;
		s++; k++;
	}
	return sum;
}

//verifica suma de paritate pentru s
//valoarea returnata va fi 1 daca este corect, 0 daca nu
byte checkParity(const msg t){
	int i = 0;
	byte sum = 0;
	for(; i< t.len; i++){
		int j = 0;
		for(; j <= 7; j++)
			if(getBit(t.payload[i], j)) sum^=1;
	}
	return sum == 0;
}

//asteapta mesajul corect(in cazul paritatii), sau il corecteaza in cazul hamming
//pentru NONE va lasa mesajul primit initial nemodificat
int waitForCorrectMessage(msg *r, int errorMode){
 	while(1) { 
		int res = recv_message(r);
		if (res < 0) {
			perror("[RECEIVER] Receive error. Exiting.\n");
			return -1;
		}
		int correct = 1;
		//doar in cazul modului de corectate paritate este necesara retrimiterea
		if(errorMode == PARITY) correct = checkParity(*r);
		if(correct) break;
		nacknowledge();
	}
	//scoate bitul de semn pentru prelucrarea comenzii
	if(errorMode == PARITY) {
		memmove((*r).payload, (*r).payload + 1, (*r).len - 1);
		(*r).len--;
	}
	//decodeaza folosind metoda hamming
	if(errorMode == HAMMING){
		hammingDecode(r);
	}
	return 0;
}

//listeaza fisierele din directorul curent
//modifica prin efecat lateral size pentru numarul de fisiere din directorul curent
//returneaza un array de strings care reprezinta numele fisierelor
char **listFiles(char *path, int *size) {
	char **fileList = (char **)calloc(1000, sizeof(char*));
	int fileCount = 0;
	DIR* directory = opendir(path);

	if(directory == NULL) {
		perror("Failure to open directory. Exiting");
		fflush(stdout);
		closedir(directory);
		return NULL;
	}
	
	//orice director are urmatoarele
	fileList[0] = ".";
	fileList[1] = "..";
	fileCount = 2;
	//in continuare, se citesc numele fisierelor de director si se adauga in matrice
	struct dirent *DirEntity;
	while((DirEntity = readdir(directory)) != NULL) {
		if(!strcmp(DirEntity->d_name, ".")) continue;
		if(!strcmp(DirEntity->d_name, "..")) continue;
		fileCount++;
		fileList[fileCount-1] = strdup(DirEntity->d_name);
	}
	closedir(directory);
	*size = fileCount;
	return fileList;
}

//citeste din fisierul cu numele "fname" si trimite datele din el
int loadAndSend(char *fname, int errorMode) {
	msg t;
	int maxSize = 1400; 	//dimensiunea maxima a pachetului, 1400 pentru NONE

	int source_miner = open(fname, O_RDONLY);
	if(source_miner < 0) {
		perror("Failed to open file.");
		return 1;
	}

	struct stat st;
	stat(fname, &st);
	//dimensiunea fisierului de trimis
	int file_size = st.st_size;

	sprintf(t.payload, "%d", file_size);
	t.len = strlen(t.payload);
	//dimensiunea fisierului este trimisa la client
	stopAndWait(t, errorMode);

	
	lseek(source_miner, 0, SEEK_SET);
	int read_response;
	
	//in cazul modului paritate dimensiunea maxima este de 1399(primul octet fiind rezervat pentru bitul de paritate calculat)
	//in cazul hamming fiecare octet este codat pe doi bytes, dimensiunea maxima fiind de 700 octeti.
	if(errorMode == PARITY) maxSize = 1399;
	else if(errorMode == HAMMING) maxSize = 700;

	while(file_size > 0) {
		//dimensiunea de citit - maxSize sau cat mai este ramas din fisier de trimis 
		int toRead = file_size;
		if(file_size - maxSize >= 0) toRead = maxSize; 
		read_response = read(source_miner, t.payload, toRead);
		file_size -= read_response;
		if(read_response<0){
			//nu reuseste citirea din fisier
			close(source_miner);
			return 2;
		}
	
		t.len = toRead;
		//trimitem frame-ul
		stopAndWait(t, errorMode);
   		t.payload[0] = '\0';
  	}
  	close(source_miner);
	return 0;
}


//primeste mesaje si scrie in fisierul cu numele "fname"
int getAndWrite(char *fname, int errorMode){
    msg r;
    int flength;

    int respLen = waitForCorrectMessage(&r, errorMode);
    if(respLen < 0) {
      perror("Receive error");
      return 1;
    }

    //dimensiunea fisierului de transferat
    flength = atoi(r.payload);
	acknowledge();

    int miner_dest = open(fname, O_WRONLY | O_CREAT, 0644);
    if(miner_dest < 0) return 1;
    int scris;
  
    while(flength > 0){
       int res = waitForCorrectMessage(&r, errorMode);
       if(res) return res;

       //scrie mesajul in fisier
       scris = write(miner_dest, r.payload, r.len);
       if(scris < 0) return 2;
       flength -= scris;
  
  	   //confirma primirea corecta a datelor
	   acknowledge();  
  	}
  close(miner_dest);
  return 0;
}


//codifica un mesaj folosind bitul de paritate; lungimea mesajului va creste cu 1
void parityEncode(msg *t){
	//bitul de paritate va ocupa primul octet din mesaj
	byte parityByte = calculateParitySum((*t).payload, (*t).len);
	memmove((*t).payload +1, (*t).payload, (*t).len);
	(*t).payload[0] = parityByte;
	(*t).len++;
}

//corecteaza un word folosind metoda hamming, rezultatul va fi octetul original
byte hammingCorrect(word w){
	//deplasez la stanga pentru a avea o indexare mai usoara
	w = w << 4;
	byte syndrome = syndrome ^ syndrome;
	
	//sindromul este calculat in functie de bitii de control
	int ctrlBit = 1, i = 1;
	for(; ctrlBit <= 8; ctrlBit *=2, i++){
		int x = 1, rank = 15;
		byte sum = 0;
		for(; x <= 12; x++, rank--)
			if(ctrlPos(x, i) && getBitWord(w, rank)) sum ^= 1;
		if(sum) syndrome = syndrome + ctrlBit;
	}

	//daca sindromul nu este 0, atunci el va indica pozitia bitului gresit(+1 din cauza indexarii)
	if(syndrome) complementBitWord(&w, 16 - syndrome);
	
	//extrag mesajul intial eliminand bitii de control
  	byte original = original ^ original;
  	int rank = 13;
  	for(i = 3; i <= 12; i++, rank--)
		if(i != 4 && i != 8) {
   	    	original = original * 2 + (getBitWord(w, rank)!=0);
  	}	 
  	return original;
}

//codifica folosind metoda hamming un octet
//rezultatul va fi un cuvant pe doi octeti
word hammingConvert(byte c) {
	word w = w ^ w;
	//intial, plasam bitii care nu sunt cei de control(orice pozitie diferita de 1,2,4,8)
	int wordRank = 13;
	int bitRank = 7;
	int poz = 3;
	for(; bitRank >= 0; wordRank--, poz++)
		if(poz == 4 || poz == 8) continue;
		else if(getBit(c, bitRank--)) setBitWord(&w, wordRank);
	
	//se calculeaza si se plaseaza bitii de control
	//pentru fiecare bit de control se calculeaza suma modulo 2 pentru toate pozitiile
	//care contin indicele sau in reprezentarea binara
    int ctrlBit = 1;
    int ctrlNo = 1;
    for(; ctrlBit <= 8; ctrlBit *=2, ctrlNo++){
	    int pos = 1, rank = 15;
	    word sum = sum ^ sum;
	    for(; pos <= 16; pos++, rank--)
	    	//daca pozitia pos este controlata de bitul ctrlNo
	    	if(ctrlPos(pos, ctrlNo) && getBitWord(w, rank))
	    		sum ^= 1;	
	    if(sum) setBitWord(&w, 16 - ctrlBit);
    }
   //mesajul este deplasat la dreapta cu 4 biti
   return w >> 4;
}

//codifica un mesaj folosind codul Hamming
//lungimea mesajului se va dubla, fiecare byte ocupand dupa codificare doi bytes
void hammingEncode(msg *t) {
	msg encoded;
	int i = 0, j = 0;
	for(i = 0; i < (*t).len; i++) {
		//codific octetul folosind metoda hamming
		//rezultatul va fi un cuvant pe doi octeti
		word w = hammingConvert((*t).payload[i]);
		int wordRank = 15;
		int bitRank = 7;
		byte one, two; 
		one = one ^ one;
		two = two ^ two;

		//transform mesajul codificat din word in doi bytes
		for(; bitRank >= 0; bitRank--, wordRank--)
			if(getBitWord(w, wordRank)) setBit(&one, bitRank);
		
		for(bitRank = 7; bitRank >= 0; bitRank--, wordRank--)
			if(getBitWord(w, wordRank)) setBit(&two, bitRank);
		//pun octetii in mesajul codificat
		encoded.payload[j] = one;
		encoded.payload[j+1] = two;
		j+=2;
	}
	//actualizez mesajul cu varianta codificata, lungimea se va dubla
	(*t).len = (*t).len * 2;
	encoded.len = (*t).len;
	memset((*t).payload, 0, (*t).len);
	memmove((*t).payload, encoded.payload, (*t).len);
}

//decodifica un mesaj folosind Hamming, luand cate 2 bytes si decodificandu-i
//la final lungimea mesajului decodificat va fi jumatate din cea initiala
void hammingDecode(msg *t){
	msg decoded;
	int i = 0, j = 0;
	
	for(; i < t->len; i+=2, j++) {
		byte one = t->payload[i];
		byte two = t->payload[i+1];
		
		word w = w ^ w;
	
		//convertesc din doi bytes intr-un word
		int contWord = 15, contByte = 7;
		for(; contByte >= 0; contByte--, contWord--)
			if(getBit(one, contByte)) setBitWord(&w, contWord);
		
		for(contByte = 7; contByte >= 0; contByte--, contWord--)
			if(getBit(two, contByte)) setBitWord(&w, contWord);

		decoded.payload[j] = hammingCorrect(w);
	}
	
	//actualizez mesajul dupa codificare
	(*t).payload[0] = '\0';
	memmove((*t).payload, decoded.payload, (*t).len/2);
	(*t).len = (*t).len / 2;
}