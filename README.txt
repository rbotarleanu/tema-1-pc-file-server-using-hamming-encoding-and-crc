Nume: Botarleanu Robert-Mihai
Grupa: 321CB
Tema 1 PC

	Pentru rezolvarea temei am ales sa realizez functii independente de modul de corectare al erorilor pentru cele 5 cerinte, prin scrierea
unor functii de trimite/primire a mesajelor care realizeaza in acelasi timp si codificare/decodificarea lor, dar si respectarea cerintelor
protocolului de tip STOP-AND-WAIT prin retrimiterea mesajului pana cand este primita o confirmare de la receptor.
	Aceste functii sunt:
- int stopAndWait(msg t, int errorMode);  care va coda mesajul t(daca este cazul) si il va retrimite pana cand clientul confirma 
receptionarea cu succes
- int waitForCorrectMessage(msg *r, int errorMode) va primi un mesaj de la client, va verifica daca acesta este corect(parity) sau il va 
corecta si decoda(Hamming). Daca mesajul este incorect serverul va trimite un NACK la client si va astepta retrimiterea unui mesaj corect.

	Ulterior, am continuat prin a rezolva cerintele pentru cazul in care nu exista corupere a legaturii de date. In acest sens am scris
functiile:
- char **listFIles(char *path, int *size) care returneaza lista de fisiere din directorul descris de path si modifica prin efect lateral 
dimensiunea directorului
- int sendLs(char **files, int fCount, int errorMode) care trimite la client numarul de fisiere si lista cu numele lor prin apelarea
functiei stopAndWait(msg t, int errorMode)
- int loadAndSend(char *fname, int errorMode) care va realiza trimiterea fisierului cu numele "fname", mai intai prin trimiterea dimensiunii 
sale si ulterior a unor mesaje de lungime maxima 700/1399/1400 (NONE/PARITY/Hamming).
- int getAndWrite(char *fname, int errorMode) va primi mesaje si le va scrie in fisierul cu numele "fname", aceasta functie fiind functional 
oglinda celei de mai sus.

	Pentru a realiza corectia erorilor folosind metoda bitului de paritate am modularizat prin scrierea unei functii ce va calcula bitul de 
paritate pentru un sir de octeti si a unei functii care verifica daca mesajul primit a fost corupt(prin calcularea sumei modulo 2 a mesajului primit, inclusiv cu bitul de paritate, daca suma este 0 atunci mesajul este corect).
	Calculul bitului de paritate s-a facut folosind operatorul xor "^" astfel: pentru fiecare octet din mesaj(pana la maximul de 1400 de 
octeti) s-a luat in considerare fiecare bit al sau folosind operatii pe biti "&" si "<<".  
	Pentru metoda de coretie a erorilor Hamming, la primirea unui mesaj codificat am preluat cate doi octeti si i-am transformat intr-un 
word pentru o prelucrare mai usoara. La trimiterea unui mesaj, fiecare octet va fi transformat in doi octeti si, atunci, lungimea mesajului 
se va dubla.
	In cazul mesajului primit codificat, am calculat pentru fiecare bit de control suma modulo 2 a bitilor pe care ii controleaza si am 
format sindromul. Daca acesta are o valoare nenula atunci el va reprezenta pozitia bitului care a fost corupt de legatura de date. Prin 
complementarea bitului respectiv folosind xor am corectat mesajul. Ulterior am extras toti bitii care nu sunt de control si am reconstruit 
octetul original.
	In cazul mesajului de trimis, in primul rand, am plasat bitii D1..D8 (cei care nu sunt de control) in pozitiile aferente(cele care nu 
sunt puteri de doi - 1,2,4,8). In cazul bitilor de control am procedat intr-o maniera similara celei de la decodificare, prin calcularea 
sumei modulo 2 pentru toate pozitiile pe care le controleaza fiecare bit(cele care contin in reprezentarea in binar bitul respectiv ca 1).
Am introdus acesti biti in cuvant. Deoarece toate operatiile au fost facute cu pozitiile 0-11, am shiftat la dreapta toti bitii din cuvant 
pentru a obtine forma folosita de protocol.

	Problema prinipala a fost organizarea codului pentru a nu fi necesare verificari la fiecare comanda a modului de corectie/detectie a
erorilor folosit. Am decis ca functiile pentru ls/cp/sn/cd/exit sa fie independente de modul de corectie/detectie folosit si ca doar 
functiile specializate pentru trimitere/receptionare de mesaj sa realizeze verificarea si codificarea/decodificarea. Astfel, structura 
programului permite chiar adaugarea unor moduri noi de trimitere si receptionare a mesajelor, fara sa fie nevoie de schimbarea 
functionalitatii serverului.


